# Ansible Role: diun

This role installs [diun](https://github.com/crazy-max/diun) server binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    diun_version: v4.21.0 # tag "latest" if you want install the latest version
    diun_arch: amd64 # amd64, 386, arm64, armv5, armv6, armv7, ppc64le, riscv64, s390x

    setup_dir: /tmp

    diun_bin_path: /usr/local/bin/diun
    diun_repo_path: https://github.com/crazy-max/diun/releases/download

This role can install the latest version of a specific version. See [available diun releases](https://github.com/crazy-max/diun/releases/), and change this variable accordingly.

    diun_version: v4.21.0 # tag "latest" if you want install the latest version

The path to the home diun directory.

    diun_bin_path: /usr/local/bin/diun

diun supports amd64, 386, arm64, armv5, armv6, armv7, ppc64le, riscv64 and s390x CPU architectures, just change for the main architecture of your CPU.

    diun_arch: amd64 # amd64, 386, arm64, armv5, armv6, armv7, ppc64le, riscv64, s390x

The path of the repository of diun.

    diun_repo_path: https://github.com/crazy-max/diun/releases/download

diun needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install diun. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: diun

## License

MIT / BSD
